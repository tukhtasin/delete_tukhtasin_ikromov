--remove a previously inserted film from the inventory and all corresponding rental records
DELETE FROM inventory
WHERE film_id = (SELECT film_id FROM film WHERE title = 'The Shawshank Redemption');
DELETE FROM rental
WHERE film_id = (SELECT film_id FROM film WHERE title = 'The Shawshank Redemption');

--Remove any records related to Alex Miller (as a customer) from all tables except "Customer" and "Inventory"
DELETE FROM payment
WHERE customer_id = (SELECT customer_id FROM customer WHERE first_name = 'Alex' AND last_name = 'Miller');
DELETE FROM rental
WHERE customer_id = (SELECT customer_id FROM customer WHERE first_name = 'Alex' AND last_name = 'Miller');

SELECT * FROM inventory WHERE film_id = (SELECT film_id FROM film WHERE title = 'The Shawshank Redemption');

SELECT * FROM payment WHERE customer_id = (SELECT customer_id FROM customer WHERE first_name = 'Alex' AND last_name = 'Miller');
